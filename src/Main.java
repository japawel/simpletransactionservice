import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.xml.ws.Endpoint;

import server.impl.TransactionSystemServiceServiceSoap;



public class Main {

	public static void main(String[] args) {
		String host = "localhost";
		String port = "5251";
		for (int i = 0; i < args.length; ++i)
		{
			if (args[i].contains("–filesystem="))
			{
				TransactionSystemServiceServiceSoap.filesystem = args[i].substring(args[i].indexOf("=")+1);
			}
			if (args[i].contains("-host="))
				host = args[i].substring(args[i].indexOf("-host=")+6);
			if (args[i].contains("-port="))
				port = args[i].substring(args[i].indexOf("-port=")+6);
		}
		String webServiceURL = "http://" + host + ":" + port + "/";
		Endpoint.publish(webServiceURL, new TransactionSystemServiceServiceSoap());
		System.out.println("Service started on http://" + host + ":" + port + " in automatic mode.");
		
		TransactionSystemServiceServiceSoap.br = new BufferedReader(new InputStreamReader(System.in));
		String command = "";
		while (!"e".equals(command))
		{
			System.out.println("m - turn to manula mode");
			System.out.println("e - exit");
			while (!"e".equals(command) && !"m".equals(command))
			{
				try {
					command = TransactionSystemServiceServiceSoap.br.readLine();
				} catch (IOException e) {
				}
			}
			if (!"e".equals(command))
			{
				TransactionSystemServiceServiceSoap.automaticMode = false;
				System.out.println("Switched to manual mode.");
				System.out.println("a - turn to automatic mode");
				System.out.println("e - exit");
			}
			while (!"e".equals(command) && !"a".equals(command))
			{
				try {
					command = TransactionSystemServiceServiceSoap.br.readLine();
//					TransactionSystemServiceServiceSoap.br.
				} catch (IOException e) {
				}
			}
			if (!"e".equals(command))
			{
				System.out.println("Switched to automatic mode.");
			}
		}
	}
}
