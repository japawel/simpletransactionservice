
package client;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

import server.TransactionSystemServiceServiceSoap;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "TransactionSystemServiceService", targetNamespace = "http://server", wsdlLocation = "file:/C:/Users/Pawel/Downloads/TransactionSystemServiceService.wsdl")
public class TransactionSystemServiceService
    extends Service
{

    private final static URL TRANSACTIONSYSTEMSERVICESERVICE_WSDL_LOCATION;
    private final static WebServiceException TRANSACTIONSYSTEMSERVICESERVICE_EXCEPTION;
    private final static QName TRANSACTIONSYSTEMSERVICESERVICE_QNAME = new QName("http://server", "TransactionSystemServiceService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("file:/C:/Users/Pawel/Downloads/TransactionSystemServiceService.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        TRANSACTIONSYSTEMSERVICESERVICE_WSDL_LOCATION = url;
        TRANSACTIONSYSTEMSERVICESERVICE_EXCEPTION = e;
    }

    public TransactionSystemServiceService() {
        super(__getWsdlLocation(), TRANSACTIONSYSTEMSERVICESERVICE_QNAME);
    }

    public TransactionSystemServiceService(WebServiceFeature... features) {
        super(__getWsdlLocation(), TRANSACTIONSYSTEMSERVICESERVICE_QNAME, features);
    }

    public TransactionSystemServiceService(URL wsdlLocation) {
        super(wsdlLocation, TRANSACTIONSYSTEMSERVICESERVICE_QNAME);
    }

    public TransactionSystemServiceService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, TRANSACTIONSYSTEMSERVICESERVICE_QNAME, features);
    }

    public TransactionSystemServiceService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public TransactionSystemServiceService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns TransactionSystemServiceServiceSoap
     */
    @WebEndpoint(name = "TransactionSystemServiceServiceSoap")
    public TransactionSystemServiceServiceSoap getTransactionSystemServiceServiceSoap() {
        return super.getPort(new QName("http://server", "TransactionSystemServiceServiceSoap"), TransactionSystemServiceServiceSoap.class);
    }
    
    @WebEndpoint(name = "TransactionSystemServiceServiceSoap")
    public TransactionSystemServiceServiceSoap getTransactionSystemServiceServiceSoap(QName portName) {
        return super.getPort(portName, TransactionSystemServiceServiceSoap.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns TransactionSystemServiceServiceSoap
     */
    @WebEndpoint(name = "TransactionSystemServiceServiceSoap")
    public TransactionSystemServiceServiceSoap getTransactionSystemServiceServiceSoap(WebServiceFeature... features) {
        return super.getPort(new QName("http://server", "TransactionSystemServiceServiceSoap"), TransactionSystemServiceServiceSoap.class, features);
    }

    private static URL __getWsdlLocation() {
        if (TRANSACTIONSYSTEMSERVICESERVICE_EXCEPTION!= null) {
            throw TRANSACTIONSYSTEMSERVICESERVICE_EXCEPTION;
        }
        return TRANSACTIONSYSTEMSERVICESERVICE_WSDL_LOCATION;
    }

}
