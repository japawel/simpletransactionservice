package client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.xml.ws.BindingProvider;

import server.ResponseType;
import server.TransactionResponse;
import server.TransactionSystemServiceServiceSoap;

public class ConsoleClient {

	private PrintStream log;
	private Map<String, TransactionSystemServiceServiceSoap> services = new HashMap<String, TransactionSystemServiceServiceSoap>();
	private Map<String, String> serviceAddresses = new HashMap<String, String>();
	private int timeout = 10000;
	
	public ConsoleClient(PrintStream log)
	{
		this.log = log;
		init();
	}
	
	private void init()
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		readServersFromConfig();
		printDefActions();
		ConsoleClientCommands consoleCommand = getCommand(br);
		while (consoleCommand != ConsoleClientCommands.EXIT)
		{
			try
			{
				switch(consoleCommand)
				{
				case HELP: printDefActions(); break;
				case ADDSERVER: addSerwer(br); break;
				case LISTSERVER : listServices(); break;
				case LISTFILES: listFiles(br); break;
				case READ: readFile(br); break;
				case STARTTR: startTransaction(br); break;
				case SETTIMEOUT: setTimeout(br); break;
				default:
					log.println("Wrong command.");
				}
				consoleCommand = getCommand(br);
			} catch (Throwable e)
			{
				log.println(e.getMessage());
				consoleCommand = ConsoleClientCommands.NONE;
			}
		}
	}
	
	private ConsoleClientCommands getCommand(BufferedReader br)
	{
		ConsoleClientCommands consoleCommand = ConsoleClientCommands.NONE;
		try{
			String command = getStringCommnad(br);
			consoleCommand = ConsoleClientCommands.valueOf(command);
		} catch (Throwable e)
		{
		}
		return consoleCommand;
	}
	
	private String getStringCommnad(BufferedReader br) throws IOException
	{
		return br.readLine().toUpperCase();
	}
	
	private void readFile(BufferedReader br) throws IOException {
		log.println("Chose server eg. JAVA:");
		listServices();
		String serviceName = getStringCommnad(br);;
		if (services.containsKey(serviceName))
		{
			log.println("Set filename: ");
			listFiles(services.get(serviceName));
			String command = br.readLine();
			readFile(command, services.get(serviceName));
		} else {
			log.println("Unknown service");
		}
	}

	private void listFiles(BufferedReader br) throws IOException {
		log.println("Chose server eg. JAVA:");
		listServices();
		String command = getStringCommnad(br);
		if (services.containsKey(command)){
			listFiles(services.get(command));
		}else {
			log.println("Unknown service");
		}
	}

	private void readFile(String filename, TransactionSystemServiceServiceSoap service)
	{
		byte[] file = client.Client.readFile(filename, service);
		FileOutputStream output = null;
		try{
			output = new FileOutputStream(new File(filename));
			output.write(file);
			log.println("File saved on disk.");
		} catch (Throwable e)
		{
			e.printStackTrace();
		} finally {
			if (output != null)
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
	private void listFiles(TransactionSystemServiceServiceSoap service)
	{
		List<String> filelist = client.Client.ls(service);
		log.println("");
		log.println("----FILES LIST STARTS----");
		for (String file : filelist)
			log.println(file);
		log.println("----FILES LIST ENDS------");
		log.println("");
	}
	
	private void startTransaction(BufferedReader br) throws IOException {
		printWriteActions();
		ConsoleClientCommands consoleCommand = getCommand(br);
		List<TransactionSystemServiceServiceSoap> serviceList = new ArrayList<TransactionSystemServiceServiceSoap>();
		List<String> fileNameList = new ArrayList<String>();
		List<byte[]> filesList = new ArrayList<byte[]>();
		while(consoleCommand != ConsoleClientCommands.WRITE && consoleCommand != ConsoleClientCommands.EXIT)
		{
			switch(consoleCommand)
			{
			case HELP: printWriteActions(); break;
			case INDICATE : getFileToWrite(serviceList, fileNameList, filesList, br); break;
			case ADDSERVER: addSerwer(br); break;
			case LISTSERVER : listServices(); break;
			case LISTFILES: listFiles(br); break;
			case READ: readFile(br); break;
			case SETTIMEOUT: setTimeout(br); break;
			case INTERRUPT: return;
			default:
				log.println("Wrong command.");
			}
			consoleCommand = getCommand(br);
		}
		if (consoleCommand != ConsoleClientCommands.EXIT)
			writeFiles(serviceList, fileNameList, filesList, br);
	}

	private void setTimeout(BufferedReader br) {
		try {
			String input = br.readLine();
			Integer intInput = Integer.valueOf(input);
			timeout = intInput;
			Set<Map.Entry<String, TransactionSystemServiceServiceSoap>> setServices = services.entrySet();
			Iterator<Map.Entry<String, TransactionSystemServiceServiceSoap>> it = setServices.iterator();
			while (it.hasNext())
			{
				Map.Entry<String, TransactionSystemServiceServiceSoap> mapEntry = (Map.Entry<String, TransactionSystemServiceServiceSoap>)it.next();
				Map<String, Object> requestContext = ((BindingProvider)mapEntry.getValue()).getRequestContext();
				requestContext.put("com.sun.xml.internal.ws.request.timeout", timeout);
			}
			log.println("Timeout changed.");
		} catch (IOException e) {
			log.println(e.getMessage());
		}
		
	}

	private void writeFiles(
			List<TransactionSystemServiceServiceSoap> serviceList,
			List<String> fileNameList, List<byte[]> filesList, BufferedReader br) {
		
		List<Long> transactionIds = new ArrayList<Long>();
		for (int i = 0; i <  fileNameList.size(); i++)
		{
			TransactionResponse response = null;
			try
			{
				response = Client.writeFile(filesList.get(i), fileNameList.get(i), serviceList.get(i));
			} catch (Throwable e)
			{
				log.println(e.getMessage());
				response = new TransactionResponse();
				response.setType(ResponseType.FAILURE);
			}
			if (response.getType() == ResponseType.SUCCESS || response.getTransactionId() >= 0L)
			{
				transactionIds.add(response.getTransactionId());
			} else 
			{
				for (int j = 0; j < transactionIds.size(); j++)
				{
					Client.refuseTransaction(transactionIds.get(j), serviceList.get(j));
				}
				log.println("Transaction rollback");
				log.println(response.getMessage());
				return;
			}
		}
		for (int i = 0; i <  transactionIds.size(); i++)
		{
			Client.acceptTransaction(transactionIds.get(i), serviceList.get(i));
		}
		log.println("Transaction accepted");
		
	}

	private void getFileToWrite(
			List<TransactionSystemServiceServiceSoap> serviceList,
			List<String> fileNameList, List<byte[]> filesList, BufferedReader br) {
		boolean ok = false;
		while (!ok)
		{
			try{
				log.println("Chosse file to write on server");
				Path currentRelativePath = Paths.get("");
				String s = currentRelativePath.toAbsolutePath().toString();
				JFileChooser fileChooser = new JFileChooser(new File(s));
				fileChooser.showOpenDialog(new JPanel());
				String filepath = fileChooser.getSelectedFile().getAbsolutePath();
				try {
					byte[] file = Files.readAllBytes(Paths.get(filepath));
					filesList.add(file);
				} catch (IOException e) {
					e.printStackTrace();
					return;
				}
				while (!ok)
				{
					log.println("Chose server eg. JAVA:");
					listServices();
					String serviceName = getStringCommnad(br);;
					if (services.containsKey(serviceName))
					{
						serviceList.add(services.get(serviceName));
						while (!ok)
						{
							log.println("Set filename: ");
							String command = br.readLine();
							fileNameList.add(command);
							ok = true;
						}
					} else {
						log.println("Unknown service");
					}
				}
			} catch (Throwable e)
			{
				e.printStackTrace();
			}
		}
		log.println("File ready to write on server. What next...");
	}

	private void printDefActions()
	{
		log.println();
		log.println("----COMMANDS---------");
		log.println(ConsoleClientCommands.HELP.getToPrint());
		log.println(ConsoleClientCommands.EXIT.getToPrint());
		log.println(ConsoleClientCommands.ADDSERVER.getToPrint());
		log.println(ConsoleClientCommands.LISTSERVER.getToPrint());
		log.println(ConsoleClientCommands.LISTFILES.getToPrint());
		log.println(ConsoleClientCommands.READ.getToPrint());
		log.println(ConsoleClientCommands.STARTTR.getToPrint());
		log.println(ConsoleClientCommands.SETTIMEOUT.getToPrint());
		log.println("----COMANDS ENDS------");
		log.println();
	}
	
	private void printWriteActions()
	{
		log.println();
		log.println("----COMMANDS---------");
		log.println(ConsoleClientCommands.HELP.getToPrint());
//		log.println(ConsoleClientCommands.EXIT.getToPrint());
		log.println(ConsoleClientCommands.ADDSERVER.getToPrint());
		log.println(ConsoleClientCommands.LISTSERVER.getToPrint());
		log.println(ConsoleClientCommands.LISTFILES.getToPrint());
		log.println(ConsoleClientCommands.INDICATE.getToPrint());
		log.println(ConsoleClientCommands.WRITE.getToPrint());
		log.println(ConsoleClientCommands.INTERRUPT.getToPrint());
		log.println(ConsoleClientCommands.SETTIMEOUT.getToPrint());
		log.println("----COMANDS ENDS------");
		log.println();
	}
	
	private void addSerwer(BufferedReader br)
	{
		try {
			log.println("Set serwer name:");
			String keyName = getStringCommnad(br);;
			log.println("Set host:");
			String host = getStringCommnad(br);
			log.println("Set port:");
			String port = getStringCommnad(br);
			
			addNewSerwer(host + ":" + port, keyName);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void addNewSerwer(String host, String keyName)
	{
		try{
			if (services.containsKey(keyName.toUpperCase()))
			{
				log.println(keyName.toUpperCase() + " service already known.");
				return;
			}
			
			Path p = Paths.get("");			
			String wsdlLocation = "file:///" + p.toAbsolutePath().toString() + "/WebContent/META-INF/wsdl/TransactionSystemServiceService.wsdl";
			TransactionSystemServiceServiceSoap service = null;
			try{
				service = client.Client.getService(wsdlLocation, "TransactionSystemServiceServiceSoap12");
			
				Map<String, Object> requestContext = ((BindingProvider)service).getRequestContext();
				requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://" + host);
				requestContext.put("com.sun.xml.internal.ws.request.timeout", timeout);
				
				client.Client.ls(service);
			} catch (Throwable e)
			{
				service = client.Client.getService(wsdlLocation, "TransactionSystemServiceServiceSoap");
				
				Map<String, Object> requestContext = ((BindingProvider)service).getRequestContext();
				requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "http://" + host);
				requestContext.put("com.sun.xml.internal.ws.request.timeout", timeout);
				client.Client.ls(service);
			}
			services.put(keyName.toUpperCase(), service);
			serviceAddresses.put(keyName.toUpperCase(), host);
			log.println(keyName + " service added.");
		} catch (Throwable e)
		{
			log.println("Can not add server: " + keyName);
			log.println(e.getMessage());
		}
	}
	
	private void listServices()
	{
		Set<Map.Entry<String, String>> setServices = serviceAddresses.entrySet();
		Iterator<Map.Entry<String, String>> it = setServices.iterator();
		log.println("");
		log.println("----SERVERS LIST STARTS----");
		while (it.hasNext())
		{
			Map.Entry<String, String> mapEntry = (Map.Entry<String, String>)it.next();
			log.println(mapEntry.getKey() + " - " + mapEntry.getValue());
		}
		log.println("----SERVERS LIST ENDS------");
		log.println("");
	}
	
	private void readServersFromConfig()
	{
		Scanner scaner = null;
		String keyName = null;
		String hostname = null;
		String port = null;
		try{
			scaner = new Scanner(new File("config"));
			String line;
			while (scaner.hasNextLine())
			{
				line = scaner.nextLine();
				if (line.contains("servicename="))
				{
					if (keyName != null)
					{
						addNewSerwer(hostname + ":" + port, keyName);
						keyName = null;
						hostname = null;
						port = null;
					}
					keyName = line.substring(line.indexOf("servicename=") + "servicename=".length());
				}
				if (line.contains("host="))
					hostname = line.substring(line.indexOf("host=") + "host=".length());
				if (line.contains("port="))
					port = line.substring(line.indexOf("port=") + "port=".length());
			}
			addNewSerwer(hostname + ":" + port, keyName);
		} catch (Throwable e)
		{
			e.printStackTrace();
		} finally {
			if (scaner != null)
				scaner.close();
		}
	}
	
}
