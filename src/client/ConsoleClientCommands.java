package client;

public enum ConsoleClientCommands {

	EXIT("exit program"),
	ADDSERVER("add new serwer"),
	LISTSERVER("list serwers"),
	LISTFILES("list serwer files"),
	READ( "read file"),
	STARTTR("begin write transaction"),
	SETTIMEOUT("set the timeout"),
	HELP("see avaiable commands"),
	NONE(""),
	/*Transaction ENUMS*/
	INDICATE("indicate file to write"),
	WRITE("write indicated files"),
	INTERRUPT("interrupt acction"),
	ACCEPT("accept transaction"),
	ROLLBACK("rollback transaction");
	
	private String description;
	
	ConsoleClientCommands(String description)
	{
		this.description = description;
	}
	
	public String getDescription()
	{
		return description;
	}
	
	public String getCommand()
	{
		return this.name();
	}
	
	public String getToPrint()
	{
		return this.name() + " - " + description;
	}
}
