package client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Base64;
import java.util.List;

import javax.xml.namespace.QName;

import server.Response;
import server.TransactionResponse;
import server.TransactionSystemServiceServiceSoap;

public class Client {
	
	public static byte[] readFile(String filepath, TransactionSystemServiceServiceSoap service)
	{
		String base64File = service.readBase64(filepath);
		byte[] readedData = null;
		try {
			readedData = Base64.getDecoder().decode(base64File);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return readedData;
	}
	
	public static List<String> ls(TransactionSystemServiceServiceSoap service)
	{
		return service.ls();
	}
	
	public static TransactionResponse writeFile(byte[] filebytes, String filepath, TransactionSystemServiceServiceSoap service)
	{
		return service.writeBase64(filepath, Base64.getEncoder().encodeToString(filebytes));
	}
	
	public static Response acceptTransaction(Long transactionId, TransactionSystemServiceServiceSoap service)
	{
		return service.acceptTransaction(transactionId);
	}
	
	public static Response refuseTransaction(Long transactionId, TransactionSystemServiceServiceSoap service)
	{
		return service.refuseTransaction(transactionId);
	}
	
	public static TransactionSystemServiceServiceSoap getService(String wsdlLocation, String portName)
	{
		TransactionSystemServiceServiceSoap service = null;
		try {
			service = new TransactionSystemServiceService(new URL(wsdlLocation)).getTransactionSystemServiceServiceSoap(new QName("http://server", portName));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		return service;
	}
}
