package server.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.ws.BindingType;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import server.Response;
import server.ResponseType;
import server.TransactionResponse;

@WebService(
		targetNamespace = "http://server", 
		serviceName  = "TransactionSystemServiceService",
		portName = "TransactionSystemServiceServiceSoap12",
		endpointInterface = "server.impl.TransactionSystemServiceServiceSoap",
		wsdlLocation = "./server/impl/TransactionSystemServiceService.wsdl"
)
@BindingType(value="http://java.sun.com/xml/ns/jaxws/2003/05/soap/bindings/HTTP/")

public class TransactionSystemServiceServiceSoap implements server.TransactionSystemServiceServiceSoap{
	public static final String MY_KEY = "server.MY_KEY";
	public static String filesystem = "filesystem";
	public static boolean automaticMode = true;
	public static BufferedReader br = null;
	private static long timeout = 300000;
	
	@Resource
	WebServiceContext webServiceContext;
	
	@WebMethod
	public List<String> ls()
	{
		File folder = new File(filesystem);
		List<String> pathsList = listFilesForFolder(folder, "");
		return pathsList;
	}
	
	@WebMethod
	public Response read(@WebParam(name = "filepath", targetNamespace = "http://server") String filepath)
	{
		File f = new File(filesystem + "/" +filepath);
        FileDataSource fileDataSource = new FileDataSource(f);
        DataHandler dataHandler = new DataHandler(fileDataSource);
        HashMap<String, DataHandler> h = new HashMap<String, DataHandler>();
        h.put(f.getName(), dataHandler);
		webServiceContext.getMessageContext().put(MessageContext.OUTBOUND_MESSAGE_ATTACHMENTS, h);
		Response response = new Response();
		response.setType(ResponseType.SUCCESS);
		return response;
	}
	
	@WebMethod
	public TransactionResponse write(@WebParam(name = "filepath", targetNamespace = "http://server") String filepath)
	{
		TransactionResponse response = new TransactionResponse();
		if (!lockIfCan(filepath)){
			response.setType(ResponseType.FAILURE);
			response.setMessage(filepath + " already locked by another transaction.");
			return response;
		}
		
		Object o = webServiceContext.getMessageContext().get(MessageContext.INBOUND_MESSAGE_ATTACHMENTS);
		if (o instanceof HashMap)
		{
			@SuppressWarnings("unchecked")
			HashMap<String, DataHandler> h = (HashMap<String, DataHandler>) o;
			Set<Map.Entry<String, DataHandler>> s = h.entrySet();
			Iterator<Map.Entry<String, DataHandler>> it = s.iterator();
			
			Map.Entry<String, DataHandler> mapEntry = (Map.Entry<String, DataHandler>)it.next();
			DataHandler dataHolder = (DataHandler) mapEntry.getValue();
			
			try{
				InputStream input = dataHolder.getInputStream();
				byte[] buffer = new byte[8 * 1024];
				try {
					File f = new File(TransactionSystemServiceServiceSoap.filesystem +  "/" + filepath + ".tmp");
					f.createNewFile();
					System.out.println(f.getAbsolutePath());
			 
					OutputStream output = new FileOutputStream(f);
					try {
						int bytesRead;
						while ((bytesRead = input.read(buffer)) != -1) {
							output.write(buffer, 0, bytesRead);
						}
					} catch (IOException e) {
						e.printStackTrace();
						throw new RuntimeException();
					} finally {
						output.close();
					}
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					throw new RuntimeException();
				} finally {
					input.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException();
			} catch (RuntimeException e)
			{
				e.printStackTrace();
				response.setType(ResponseType.FAILURE);
				response.setMessage(filepath + " can't be write.");
				return response;
			}
		}
		response.setType(ResponseType.SUCCESS);
		response.setTransactionId(Transactions.getTransactions().getNextId(filepath));
		return response;
	}
	
	private Boolean lockIfCan(String filepath) {
		String[] paths = filepath.split("/");
		String path = TransactionSystemServiceServiceSoap.filesystem;
		for (int i = 0; i < paths.length-1; i++)
		{
			path += "/" + paths[i];
			File pathFile = new File(path);
			if (!pathFile.exists())
				pathFile.mkdirs();
		}
		DateFormat format = new SimpleDateFormat("MMMM d, yyyy kk:mm:ss", Locale.ENGLISH);
		File f = new File(TransactionSystemServiceServiceSoap.filesystem +  "/" + filepath + ".lock");
		if (f.exists())
		{
			Scanner sc = null;
			try {
				sc = new Scanner(f);
				String str = sc.nextLine();
				Date d = format.parse(str);
//				DateFormat.parse(str);
//				Date d = new Date(str);
				Date curr = new Date();
				if (curr.getTime() - d.getTime() < timeout)
				{
					return false;
				}
				f.delete();
			} catch (Throwable e) {
				e.printStackTrace();
				return false;
			} finally {
				if (sc != null)
					sc.close();
			}
		}
		PrintStream ps = null;
		try {
			f.createNewFile();
			ps = new PrintStream(f);
			ps.println(format.format(new Date()));
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (ps != null)
				ps.close();
		}
		return true;
	}

	@Override
	public Response acceptTransaction(@WebParam(name = "transactionId", targetNamespace = "http://server") Long transactionId) {
		
		Response response = new Response();
		try
		{
			String filename = Transactions.getTransactions().getFileForTransaction(transactionId);
			File f = new File(TransactionSystemServiceServiceSoap.filesystem +  "/" + filename);
			if (f.exists())
			{
				Path p = f.toPath();
				Files.delete(p);
			}
			if (f.exists())
				f.delete();
			f= new File(TransactionSystemServiceServiceSoap.filesystem +  "/" + filename + ".tmp");
			if (f.exists())
				f.renameTo(new File(TransactionSystemServiceServiceSoap.filesystem +  "/" + filename));
			f = new File(TransactionSystemServiceServiceSoap.filesystem +  "/" + filename+ ".lock");
			if (f.exists())
				f.delete();
			response.setType(ResponseType.SUCCESS);
		} catch (Throwable e)
		{
			e.printStackTrace();
			response.setType(ResponseType.FAILURE);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@Override
	public Response refuseTransaction(@WebParam(name = "transactionId", targetNamespace = "http://server") Long transactionId) {
		Response response = new Response();
		try
		{
			String filename = Transactions.getTransactions().getFileForTransaction(transactionId);
			File f= new File(TransactionSystemServiceServiceSoap.filesystem +  "/" + filename + ".tmp");
			f.delete();
			f = new File(TransactionSystemServiceServiceSoap.filesystem +  "/" + filename+ ".lock");
			f.delete();
			response.setType(ResponseType.SUCCESS);
		} catch (Throwable e)
		{
			e.printStackTrace();
			response.setType(ResponseType.FAILURE);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	private List<String> listFilesForFolder(final File folder, String prevPath) {
	    List<String> paths = new ArrayList<String>();
		for (final File fileEntry : folder.listFiles()) {
	        if (fileEntry.isDirectory()) {
	        	paths.addAll(listFilesForFolder(fileEntry, prevPath + fileEntry.getName() + "/"));
	        } else {
	        	if (!fileEntry.getName().endsWith(".tmp") && !fileEntry.getName().endsWith(".lock"))
	        		paths.add(prevPath + fileEntry.getName());
	        }
	    }
		return paths;
	}

	@Override
	public TransactionResponse writeBase64(@WebParam(name = "filepath", targetNamespace = "http://server") String filepath, @WebParam(name = "content", targetNamespace = "http://server") String content) {
		TransactionResponse response = new TransactionResponse();
		if (!lockIfCan(filepath)){
			response.setType(ResponseType.FAILURE);
			response.setMessage(filepath + " already locked by another transaction.");
			return response;
		}
		
		File f = new File(TransactionSystemServiceServiceSoap.filesystem +  "/" + filepath + ".tmp");
		try {
			f.createNewFile();
			OutputStream output = new FileOutputStream(f);
			try {
				byte[] buffer = Base64.getDecoder().decode(content);
				output.write(buffer);
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException();
			} finally {
				output.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			f = new File(TransactionSystemServiceServiceSoap.filesystem +  "/" + filepath+ ".lock");
			f.delete();
			response.setType(ResponseType.FAILURE);
			response.setMessage(e.getMessage());
			
			return response;
		}
		if (!automaticMode)
		{
			System.out.println(filepath + " was saved on filesystem. Accept? y or n");
			String command = "";
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			while (!"y".equals(command) && !"yes".equals(command) && !"n".equals(command) && !"no".equals(command))
			{
				try {
//					br.reset();
//					br.mark(-1);
					command = br.readLine();
				} catch (IOException e) {
				}
			}
			if ("n".equals(command) || "no".equals(command))
			{
				f= new File(TransactionSystemServiceServiceSoap.filesystem +  "/" + filepath + ".tmp");
				if (f.exists())
					f.renameTo(new File(TransactionSystemServiceServiceSoap.filesystem +  "/" + filepath));
				f = new File(TransactionSystemServiceServiceSoap.filesystem +  "/" + filepath + ".lock");
				if (f.exists())
					f.delete();
				response.setType(ResponseType.FAILURE);
				response.setMessage("Transaction canceled by Administrator.");
				return response;
			}
		}	
		response.setType(ResponseType.SUCCESS);
		response.setTransactionId(Transactions.getTransactions().getNextId(filepath));
		return response;
	}

	@Override
	public String readBase64(@WebParam(name = "filepath", targetNamespace = "http://server") String filepath) {
		String encodedFile = "";
		try {
			Path path = Paths.get(filesystem + "/" +filepath);
			encodedFile += Base64.getEncoder().encodeToString(Files.readAllBytes(path));
		} catch (IOException e) {
			e.printStackTrace();
			encodedFile = null;
		} 
		return encodedFile;
	}
}
