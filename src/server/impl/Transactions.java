package server.impl;

import java.util.HashMap;
import java.util.Map;

public class Transactions {
	
	private static Long sequence;
	private static Map<Long, String> transactionsMap;
	private static Transactions transactions;
	
	private Transactions()
	{
		sequence = 0L;
		transactionsMap = new HashMap<Long, String>();
	}
	
	public static Transactions getTransactions()
	{
		if (transactions == null)
			transactions = new Transactions();
		
		return transactions;
	}
	
	public Long getNextId(String forFile)
	{
		++sequence;
		transactionsMap.put(sequence, forFile);
		return sequence;
	}
	
	public String getFileForTransaction(Long transactionId)
	{
		return transactionsMap.remove(transactionId);
	}
}
