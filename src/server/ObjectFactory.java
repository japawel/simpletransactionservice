
package server;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the serwer package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: serwer
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReadBase64Response }
     * 
     */
    public ReadBase64Response createReadBase64Response() {
        return new ReadBase64Response();
    }

    /**
     * Create an instance of {@link RefuseTransactionResponse }
     * 
     */
    public RefuseTransactionResponse createRefuseTransactionResponse() {
        return new RefuseTransactionResponse();
    }

    /**
     * Create an instance of {@link Response }
     * 
     */
    public Response createResponse() {
        return new Response();
    }

    /**
     * Create an instance of {@link Ls }
     * 
     */
    public Ls createLs() {
        return new Ls();
    }

    /**
     * Create an instance of {@link AcceptTransactionResponse }
     * 
     */
    public AcceptTransactionResponse createAcceptTransactionResponse() {
        return new AcceptTransactionResponse();
    }

    /**
     * Create an instance of {@link WriteBase64 }
     * 
     */
    public WriteBase64 createWriteBase64() {
        return new WriteBase64();
    }

    /**
     * Create an instance of {@link AcceptTransaction }
     * 
     */
    public AcceptTransaction createAcceptTransaction() {
        return new AcceptTransaction();
    }

    /**
     * Create an instance of {@link LsResponse }
     * 
     */
    public LsResponse createLsResponse() {
        return new LsResponse();
    }

    /**
     * Create an instance of {@link WriteBase64Response }
     * 
     */
    public WriteBase64Response createWriteBase64Response() {
        return new WriteBase64Response();
    }

    /**
     * Create an instance of {@link TransactionResponse }
     * 
     */
    public TransactionResponse createTransactionResponse() {
        return new TransactionResponse();
    }

    /**
     * Create an instance of {@link Write }
     * 
     */
    public Write createWrite() {
        return new Write();
    }

    /**
     * Create an instance of {@link RefuseTransaction }
     * 
     */
    public RefuseTransaction createRefuseTransaction() {
        return new RefuseTransaction();
    }

    /**
     * Create an instance of {@link Read }
     * 
     */
    public Read createRead() {
        return new Read();
    }

    /**
     * Create an instance of {@link WriteResponse }
     * 
     */
    public WriteResponse createWriteResponse() {
        return new WriteResponse();
    }

    /**
     * Create an instance of {@link ReadResponse }
     * 
     */
    public ReadResponse createReadResponse() {
        return new ReadResponse();
    }

    /**
     * Create an instance of {@link ReadBase64 }
     * 
     */
    public ReadBase64 createReadBase64() {
        return new ReadBase64();
    }

}
