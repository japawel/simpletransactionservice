import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class Config {

	private static volatile Config instance = null;
	private static String javaWsdlLocation = "";
	
	public static Config getInstance()
	{
		if(instance == null)
		{
			synchronized(Config.class)
			{
				if(instance == null)
				{
					instance = new Config();
				}
			}
		}
		return instance;
	}
	
	private Config()
	{
		readConfigFile();
	}
	
	public void readConfigFile()
	{
		File configfile = new File("config");
		Scanner s = null;
		try {
			s = new Scanner(configfile);
			while (s.hasNextLine())
			{
				String line = s.nextLine();
				if (line.contains("JAVA_WSDL="))
					javaWsdlLocation = line.substring(line.indexOf("JAVA_WSDL=") + 10);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (s != null)
				s.close();
		}
	}

	public String getJavaWsdlLocation() {
		return javaWsdlLocation;
	}
}
